﻿$(document).ready(function(){

$('#slider-1').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    navText: [
      "<img class='nav-text img-responsive' src='img/icons/arrow1.png'>",
      "<img class='nav-text img-responsive' src='img/icons/arrow.png'>"
      ],
    items: 1,
    autoplay: true,
    autoplayTimeout: 6000,
    dots: true,
    URLhashListener:true,
    startPosition: 'URLHash'
})

$('#slider-2').owlCarousel({
    loop:true,
    margin:0,
    nav: false,
    navText: [
      "<img class='nav-text img-responsive' src='img/icons/arrow1.png'>",
      "<img class='nav-text img-responsive' src='img/icons/arrow.png'>"
      ],
    items: 1,
    autoplay: true,
    autoplayTimeout: 5000,
    dots: true
})

$('#slider-3').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    navText: [
      "<img class='nav-text img-responsive' src='img/icons/arrow3.png'>",
      "<img class='nav-text img-responsive' src='img/icons/arrow4.png'>"
      ],
    autoplay: true,
    autoplayTimeout: 5000,
    dots: false,
    responsive:{
        0:{
            items:1
        },
        200:{
            items:2
        },
        400:{
            items:3
        },
        600:{
            items:4
        },
        1000:{
            items:5
        }
    }
})
	$(window).click(function() {
	       // $(".up").css("display","none");
	});

	$(".navigation-1 li").click(function(){
		$(".up").css("display","none");
		$(this).find(".up").css("display","block");
	});

	$(document).find("#slider-1 .owl-dots").addClass("navigation-1");
	$(document).find("#slider-1 .owl-dots .owl-dot:eq(0) ").html("<a href='#sa'><div>Автохимия<br/>и автокосметика</div> </a><div class='up'><i class='fa fa-angle-up'></i></div>");
	$(document).find("#slider-1 .owl-dots .owl-dot:eq(1) ").html("<a href='#'><div>Детейлинг</div> </a><div class='up'><i class='fa fa-angle-up'></i></div>");
	$(document).find("#slider-1 .owl-dots .owl-dot:eq(2) ").html("<a href='#'><div>Клининг</div> </a><div class='up'><i class='fa fa-angle-up'></i></div>");
	$(document).find("#slider-1 .owl-dots .owl-dot:eq(3) ").html("<a href='#'><div>Агропромышленный<br/> комплекс</div> </a><div class='up'><i class='fa fa-angle-up'></i></div>");
	$(document).find("#slider-1 .owl-dots .owl-dot:eq(4) ").html("<a href='#'><div>Пищевая<br/> промышленность</div> </a><div class='up'><i class='fa fa-angle-up'></i></div>");

    var leftcolHeight = $('.leftcol').height();
    var rightcolHeight = $('.rightcol').innerHeight();
    if (rightcolHeight > leftcolHeight) {
        $('.leftcol').height(rightcolHeight - 200);
    } else {
        $('.leftcol').removeAttr('height');
    }

    if ($(window).innerWidth() <= 768) {
        $('.sec-product .leftcol').insertAfter($('.sec-product h1'));
    }

    $('.big-image-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.small-images-slider'
    });
    $('.small-images-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.big-image-slider',
        dots: false,
        prevArrow: '<span class="product-arrow prev-arrow"></span>',
        nextArrow: '<span class="product-arrow next-arrow"></span>',
        focusOnSelect: true
    });

    $('.about-slider').slick({
        dots: true,
        arrows: false
    });

    if ($(window).innerWidth() <= 1058) {
        var slidesNum = 1;
    } else {
        var slidesNum = 3;
    }

    $('.about-photo-slider, .about-video-slider').slick({
        slidesToShow: slidesNum,
        slidesToScroll: 1,
        dots: false,
        prevArrow: '<span class="product-arrow prev-arrow"></span>',
        nextArrow: '<span class="product-arrow next-arrow"></span>',
    });

    if ($(window).innerWidth() > 1058) {
        $('.document-single').each(function() {
            $(this).find('.document-single-img').height($(this).innerHeight() - 24);
        });
    }

    if ($('.dealers-form').length) {
        $('.dealers-form input[type="tel"]').inputmask({
      	    mask: '+7 (999) 999-99-99',
      	    showMaskOnHover: false,
      	    showMaskOnFocus: true,
      	});
    }
});

$(window).resize(function() {
    var leftcolHeight = $('.leftcol').height();
    var rightcolHeight = $('.rightcol').innerHeight();
    if (rightcolHeight > leftcolHeight) {
        $('.leftcol').height(rightcolHeight - 200);
    } else {
        $('.leftcol').removeAttr('height');
    }
});

$('.map-marker-single').click(function() {

    var markerLeft = $(this).position().left;
    var markerTop = $(this).position().top;
    $(this).next('.dealer-info').css({
        left: markerLeft,
        top: markerTop
    });

    if ($(this).next('.dealer-info').find('.tab.current').is(':empty')) {
        var manager = $(this).next('.dealer-info').find('.tab.current').attr('data-manager');
        var managerInfo = $('.managers-list [data-manager=' + manager + ']').html();
        $(this).next('.dealer-info').find('.tab.current').prepend(managerInfo);
    }

    $('.map-marker-single').removeClass('marker-visible');
    $(this).addClass('marker-visible');
});

$('.dealer-info-close').click(function() {

    $(this).closest('.dealer-info').prev('.map-marker-single').removeClass('marker-visible');
});

$('.dealer-info-nav a').click(function() {

    if (!$(this).hasClass('current')) {
        var $thisParent = $(this).closest('.dealer-info');
        var tab = $(this).attr('data-tab');
        $thisParent.find('.dealer-info-nav a').removeClass('current');
        $thisParent.find('.dealer-info-tabs .tab').removeClass('current');
        $(this).addClass('current');
        $thisParent.find('.dealer-info-tabs .' + tab).addClass('current');

        if ($thisParent.find('.tab.current').is(':empty')) {
            var manager = $thisParent.find('.tab.current').attr('data-manager');
            var managerInfo = $('.managers-list [data-manager=' + manager + ']').html();
            $thisParent.find('.tab.current').prepend(managerInfo);
        }
    }

    return false;
});

$('.product-sertificates').click(function() {

    $(this).toggleClass('opened');
    if ($('.product-sertificates-block').css('display') == 'block') {
        $('.product-sertificates-block').slideUp(300);
    } else {
        $('.product-sertificates-block').slideDown(300);
    }

    return false;
});

$(document).ready(function() {

    $('.product-sertificates-block').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Загрузка изображения #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1]
		},
		image: {
			tError: '<a href="%url%">Изображение #%curr%</a> не может быть загружено.'
		}
	});
});

// Cart
$('.control-decrease').click(function() {
    var input = $(this).closest('.cart-table-controls').find('.cart-table-quantity');
    if (parseInt(input.val()) > 0) {
        input.val(parseInt(input.val()) - 1);
    } else {
        input.val(0);
    }
});

$('.control-increase').click(function() {
    var input = $(this).closest('.cart-table-controls').find('.cart-table-quantity');
    input.val(parseInt(input.val()) + 1);
});

$('.cart-table-quantity').change(function() {
    var quantity = parseInt($(this).val());
    if (quantity < 0) {
        $(this).val(0);
    }
});

$('.product-variation span').click(function() {

    if (!$(this).hasClass('active')) {
        $('.product-variation span').removeClass('active');
        $(this).addClass('active');
    }
});
// End Cart

$('.popup').click(function() {

    var target = $(this).attr('href');

    $.magnificPopup.open({
        items: {
            src: target
        },
        type: 'inline'
    });

    return false;
});

jQuery(function(){
  jQuery('a[href^="#"]').anchorScroll();
})

// anchorScroll
jQuery.fn.anchorScroll = function(options){
  var options = jQuery.extend({
    duration: 300,
    easing: 'swing',
    locationReplace: true
  }, options);

  return this.each(function(){
    var $this = jQuery(this),
      _hash = this.hash,
      _boxTo = jQuery(this.hash),
      _duration = options.duration,
      _easing = options.easing,
      _locationReplace = options.locationReplace;

    if(_boxTo.length && _boxTo.is(':visible')){
      $this.bind('click', function(){
        jQuery('html, body').animate({scrollTop: _boxTo.offset().top}, {queue: false, easing: _easing, duration: _duration, complete: function() {
          if(_locationReplace) location.hash = _hash.replace('#', '');
        }});
        return false;
      })
    }
  })
}
